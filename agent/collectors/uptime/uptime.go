package uptime

import (
	"gitlab.com/hds-agent/hds-agent/agent/collectors"
)

// Run returns uptime Metric results
func Run() ([]*collectors.MetricResult, error) {
	var (
		data []byte
		err  error
	)
	if data, err = loader(); err != nil {
		return nil, err
	}
	return preformatter(data)
}
