package diskusage

import (
	"gitlab.com/hds-agent/hds-agent/agent/collectors"
)

// Run returns disk usage metrics
func Run() ([]*collectors.MetricResult, error) {
	data, err := loader()
	if err != nil {
		return nil, err
	}

	return preformatter(data)
}
